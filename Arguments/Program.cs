﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arguments
{
    class Program
    {
        static int Main(string[] args)
        {
            foreach (string asd in args)
                Console.WriteLine(asd);
            if (args.Length == 0)
            {
                Console.WriteLine("ERROR! Please enter a Task (like TaskA) and arguments for it.");
                Console.ReadKey();
                return 1;
            }
            Console.WriteLine("String task: {0}", args[0]);
            switch (args[0])
            {
                case "TaskA":
                    TaskA(args);
                    break;
                case "TaskB":
                    TaskB(args);
                    break;
                case "TaskC":
                    TaskC(args);
                    break;
                default:
                    Console.WriteLine("ERROR! Task not found.");
                    break;
            }

            Console.ReadKey();
            return 0;
        }




        static void TaskA(string[] args)
        {

            string input = "";
            if (args[1] == "-c")
            {
                input = Console.ReadLine();
            }
            else if (args[1] == "-f")
            {
                if (args.Length < 3)
                {
                    Console.WriteLine("ERROR! No path provided");
                    Console.ReadKey();
                    return;
                }
                if (File.Exists(args[2]))
                    input = File.ReadAllText(args[2]);
                else
                {
                    Console.WriteLine("ERROR! File not found!");
                    Console.ReadKey();
                    return;
                }
            }
            else
            {
                Console.WriteLine("ERROR! This argument must be -f (for file input) or -c (for console input)");
                Console.ReadKey();
                return;
            }

            string[] numsStr = input.Split(' ');
            int count = 0;
            double sum = 0;
            foreach (string n in numsStr)
            {
                int num = 0;
                if (Int32.TryParse(n, out num))
                {
                    sum += num;
                    count++;
                }
            }
            if (count != 0)
                Console.WriteLine("Task 1 result: {0}", sum / count);
            else
                Console.WriteLine("Task 1 result: no integer provided");

            return;
        }

        static void TaskB(string[] args)
        {

            string input = "";
            if (args[1] == "-c")
            {
                input = Console.ReadLine();
            }
            else if (args[1] == "-f")
            {
                if (args.Length < 3)
                {
                    Console.WriteLine("ERROR! No path provided");
                    Console.ReadKey();
                    return;
                }
                if (File.Exists(args[2]))
                    input = File.ReadAllText(args[2]);
                else
                {
                    Console.WriteLine("ERROR! File not found!");
                    Console.ReadKey();
                    return;
                }
            }
            else
            {
                Console.WriteLine("ERROR! This argument must be -f (for file input) or -c (for console input)");
                Console.ReadKey();
                return;
            }

            input = input.Replace(',', '.');
            string[] numsStr = input.Split(' ');
            List<double> nums = new List<double>();

            foreach (string n in numsStr)
            {
                double num = 0;
                if (Double.TryParse(n, out num))
                {
                    nums.Add(num);
                    Console.WriteLine(num);
                }
                else
                {
                    throw new Exception("ERROR! Only numbers allowed");
                }
            }

            Console.WriteLine("Task 2 result: Geometric - {0}; Harmonic - {1}; ", GeometricAverage(nums), HarmonicAverage(nums));

            return;
        }

        static void TaskC(string[] args)
        {
            if (args.Length < 3)
            {
                Console.WriteLine("ERROR! Need 2 arguments!");
                Console.ReadKey();
                return;
            }
            else
            {
                int number = 0, osn = 0;
                if (Int32.TryParse(args[1], out number) && Int32.TryParse(args[2], out osn))
                    Console.WriteLine("Task 3 result: Number {0} to {1} base = {2}", number, osn, InvertedGornerScheme(number, osn));
                else
                {
                    Console.WriteLine("ERROR! Invalid arguments!");
                    Console.ReadKey();
                    return;
                }
            }
            return;
        }


        /// <summary>
        /// Calculates geometric average.
        /// </summary>
        /// <remarks>
        /// See https://ru.wikipedia.org/wiki/%D0%A1%D1%80%D0%B5%D0%B4%D0%BD%D0%B5%D0%B5_%D0%B3%D0%B5%D0%BE%D0%BC%D0%B5%D1%82%D1%80%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%BE%D0%B5
        /// </remarks>
        /// <param name="numbers">The nums.</param>
        /// <returns>Average</returns>
        static double GeometricAverage(List<double> numbers)
        {
            double result = 1;

            foreach (double num in numbers)
            {
                result *= num;
            }
            result = Math.Pow(result, 1.0 / numbers.Count);

            return result;
        }

        /// <summary>
        /// Calculates harmonics average.
        /// </summary>
        /// <remarks>
        /// See https://ru.wikipedia.org/wiki/%D0%A1%D1%80%D0%B5%D0%B4%D0%BD%D0%B5%D0%B5_%D0%B3%D0%B0%D1%80%D0%BC%D0%BE%D0%BD%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%BE%D0%B5
        /// </remarks>
        /// <param name="numbers">Numbers</param>
        /// <returns>Average</returns>
        static double HarmonicAverage(List<double> numbers)
        {
            double result = 0;

            foreach (double num in numbers)
            {
                result += 1.0 / num;
            }
            result = numbers.Count / result;

            return result;
        }

        /// <summary>
        /// Converts int number to given base
        /// </summary>
        /// <param name="number">The number.</param>
        /// <param name="osn">The base of number.</param>
        /// <returns>String with number in that base</returns>
        static string InvertedGornerScheme(int number, int osn)
        {
            int n = number, r = 0;
            while (n > 0)
            {
                n /= osn; r++;
            }
            string result = "";
            while (number > 0)
            {
                result = result.Insert(0, ((number % osn < 10) ? ((char)(number % osn + '0')).ToString() : ((char)(number % osn + 'A' - 10)).ToString()));
                r--;
                number /= osn;
            }
            return result;
        }
    }
}
